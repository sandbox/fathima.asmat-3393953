<?php

namespace Drupal\author_bulk_assignment\Plugin\views\field;

use Drupal\author_bulk_assignment\Plugin\views\field\traits\AuthorAssignmentBulkFormTrait;
use Drupal\node\Plugin\views\field\NodeBulkForm;

/**
 * Defines a user operations bulk form element.
 *
 * @ViewsField("author_assignment_node_bulk_form")
 */
class AuthorAssignmentNodeBulkForm extends NodeBulkForm {

  use AuthorAssignmentBulkFormTrait;

}
