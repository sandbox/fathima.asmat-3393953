<?php
/**
 * @file
 * Provide views data for author_bulk_assignment.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function author_bulk_assignment_views_data_alter(array &$data) {
  foreach (\Drupal::entityTypeManager()
    ->getDefinitions() as $entity_type => $entity_info) {
    $base_table = $entity_info->getBaseTable();
    if (!empty($data[$base_table][$entity_type . '_bulk_form'])) {
      $bulk_form_view_field = $entity_type === 'node' ? 'author_assignment_node_bulk_form' : 'author_assignment_entity_bulk_form';
      $data[$base_table][$entity_type . '_bulk_form']['field']['id'] = $bulk_form_view_field;
    }
  }
}
