<?php

namespace Drupal\author_bulk_assignment\Form;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\system\Entity\Action;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a setting form to set entity types for author assignment.
 *
 * @package Drupal\author_bulk_assignment\Form
 */
class SettingsForm extends FormBase {

  /**
   * Form's config settings name.
   *
   * @const string
   */
  const CONFIG_NAME = 'author_bulk_assignment.settings';

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new \Drupal\author_bulk_assignment\Form\SettingsForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, MessengerInterface $messenger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'author_bulk_assignment_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);

    $definitions = $this->entityTypeManager->getDefinitions();
    $entity_types = [];
    $selected_entity_types = [];
    foreach ($definitions as $definition) {
      if ($this->isApplicableEntity($definition)) {
        $entity_types[$definition->id()] = $definition->getLabel();
        if ($config && $config->get('types') && in_array($definition->id(), $config->get('types'))) {
          $selected_entity_types[] = $definition->id();
        }
      }
    }
    $form['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity types'),
      '#description' => $this->t('Select the entity types that should have a bulk action for assigning the content to a different author form the admin listings'),
      '#options' => $entity_types,
      '#default_value' => $selected_entity_types,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected_types = [];
    $unselected_actions = [];
    $config = $this->configFactory->getEditable(self::CONFIG_NAME);

    $definitions = $this->entityTypeManager->getDefinitions();
    foreach ($definitions as $definition) {
      $action_id = $definition->id() . '_author_bulk_assignment_action';
      if ($this->isApplicableEntity($definition) && in_array($definition->id(), $this->removeEmptyValue($form_state->getValue('entity_types')))) {
        $selected_types[] = $definition->id();

        // For each selected entity type create a system action config entity.
        if (empty($config->get('types')) || !in_array($definition->id(), $config->get('types'))) {
          $action = Action::create([
            'id' => $action_id,
            'plugin' => 'entity:author_bulk_assignment_action:' . $definition->id(),
            'label' => $this->t('Assign bulk :type_label to author', [':type_label' => strtolower($definition->getSingularLabel())]),
            'type' => $definition->id(),
          ]);
          try {
            $saved = $action->save();
          }
          catch (EntityStorageException $e) {
            $this->messenger->addError($this->t('Failed to create author_bulk_assignment_action for :type', [
              ':type' => $definition->id(),
            ]));
            return;
          }

          if ($saved) {
            $this->messenger->addMessage($this->t('Action :config installed successfully.', [
              ':config' => $action_id,
            ]));
          }
        }
      }
      else {
        $unselected_actions[] = $action_id;
      }
    }

    // Save config with the selected types.
    $config->set('types', $selected_types)->save();

    // Remove existing actions for unselected entities.
    $actions_storage = $this->entityTypeManager->getStorage('action');
    $actions_to_delete = $actions_storage->getQuery()
      ->condition('id', $unselected_actions, 'IN')
      ->execute();
    if ($actions_to_delete) {
      $action_entities = $actions_storage->loadMultiple($actions_to_delete);
      foreach ($action_entities as $action_entity) {
        if ($action_entity instanceof Action) {
          try {
            $action_entity->delete();
          }
          catch (EntityStorageException $e) {
            $this->messenger->addError($this->t('Failed to remove :action_id', [
              ':action_id' => $action_entity->id(),
            ]));
            return;
          }
        }
      }

      $this->messenger->addMessage($this->t('Actions updated successfully.'));
    }
  }

  /**
   * Indicates whether the action can be used for the provided entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return bool
   *   TRUE if the entity type can be used, FALSE otherwise.
   */
  protected function isApplicableEntity(EntityTypeInterface $entity_type): bool {
    // Any entity type that has an author field.
    // Either "owner" OR "uid" field.
    return $entity_type->hasKey('uid') || $entity_type->hasKey('owner');
  }

  /**
   * Helper function to filter empty value in an array.
   *
   * @param array $array
   *   The array to check for empty values.
   *
   * @return array
   *   The array without empty values.
   */
  protected function removeEmptyValue(array $array): array {
    return array_filter($array, function ($value) {
      return !empty($value);
    });
  }

}
