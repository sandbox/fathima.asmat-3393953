<?php

namespace Drupal\author_bulk_assignment\Plugin\views\field\traits;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Entity\Render\EntityTranslationRenderTrait;

/**
 * Trait for Node and Entity bulk forms.
 *
 * @see \Drupal\author_bulk_assignment\Plugin\views\field\AuthorAssignmentEntityBulkForm
 * @see \Drupal\author_bulk_assignment\Plugin\views\field\AuthorAssignmentNodeBulkForm
 */
trait AuthorAssignmentBulkFormTrait {

  use EntityTranslationRenderTrait;

  /**
   * Assignee user reference field name.
   *
   * @var string
   */

  protected static string $assigneeFieldName = 'assignee_uid';

  /**
   * {@inheritDoc}
   */
  public function viewsForm(&$form, FormStateInterface $form_state) {
    // This function is largely a duplication of the
    // Drupal\views\Plugin\views\field\BulkForm::viewsForm() method,
    // with the only modification being the inclusion of the assignee
    // author reference field in the bulk operations.
    // -----------------------------------------------------------//
    // Make sure we do not accidentally cache this form.
    // @todo Evaluate this again in https://www.drupal.org/node/2503009.
    $form['#cache']['max-age'] = 0;

    // Add the tableselect javascript.
    $form['#attached']['library'][] = 'core/drupal.tableselect';
    $use_revision = array_key_exists('revision', $this->view->getQuery()
      ->getEntityTableInfo());

    // Only add the bulk form options and buttons if there are results.
    if (!empty($this->view->result)) {
      // Render checkboxes for all rows.
      $form[$this->options['id']]['#tree'] = TRUE;
      foreach ($this->view->result as $row_index => $row) {
        $entity = $this->getEntityTranslationByRelationship($this->getEntity($row), $row);

        $form[$this->options['id']][$row_index] = [
          '#type' => 'checkbox',
          // We are not able to determine a main "title" for each row, so we can
          // only output a generic label.
          '#title' => $this->t('Update this item'),
          '#title_display' => 'invisible',
          '#default_value' => !empty($form_state->getValue($this->options['id'])[$row_index]) ? 1 : NULL,
          '#return_value' => $this->calculateEntityBulkFormKey($entity, $use_revision),
        ];
      }

      // Replace the form submit button label.
      $form['actions']['submit']['#value'] = $this->t('Apply to selected items');
      $form['actions']['submit']['#weight'] = 0;

      // Ensure a consistent container for
      // filters/operations in the view header.
      $form['header'] = [
        '#type' => 'container',
        '#weight' => -100,
      ];

      // Build the bulk operations action widget for the header.
      // Allow themes to apply .container-inline on this separate container.
      $bulk_options = $this->getBulkOptions();
      $form['header'][$this->options['id']] = [
        '#type' => 'container',
      ];
      $form['header'][$this->options['id']]['action'] = [
        '#type' => 'select',
        '#title' => $this->options['action_title'],
        '#options' => $bulk_options,
      ];

      $action_id = $this->getEntityTypeId() . '_author_bulk_assignment_action';
      if (in_array($action_id, array_keys($bulk_options))) {
        $form['header'][$this->options['id']][self::$assigneeFieldName] = [
          '#type' => 'entity_autocomplete',
          '#title' => $this->t('Assign to'),
          '#target_type' => 'user',
          '#selection_settings' => [
            'include_anonymous' => FALSE,
          ],
          '#validate_reference' => FALSE,
          '#size' => '20',
          '#maxlength' => '60',
          '#states' => [
            'visible' => [
              ':input[name="action"]' => ['value' => $action_id],
            ],
          ],
        ];
        $form['#attached']['library'][] = 'author_bulk_assignment/drupal.author_bulk_assignment.admin';
      }
      // Duplicate the form actions into the action container in the header.
      $form['header'][$this->options['id']]['actions'] = $form['actions'];
    }
    else {
      // Remove the default actions build array.
      unset($form['actions']);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function viewsFormSubmit(&$form, FormStateInterface $form_state) {
    if ($form_state->get('step') == 'views_form_views_form') {
      // Filter only selected checkboxes. Use the actual user input rather than
      // the raw form values array, since the site data may change before the
      // bulk form is submitted, which can lead to data loss.
      $user_input = $form_state->getUserInput();
      $selected = array_filter($user_input[$this->options['id']]);
      $action_id = $form_state->getValue('action');
      $action = $this->actions[$action_id];
      $entities = [];
      $count = 0;

      $assignee_uid = $form_state->getValue(self::$assigneeFieldName);
      $bulk_author_assign = ($action_id === $this->getEntityTypeId() . '_author_bulk_assignment_action' && $assignee_uid);
      if ($bulk_author_assign) {
        $action->getPlugin()->setAssignee($assignee_uid);
        $this->actions[$action_id] = $action;

        // Exclude entities inaccessible to the selected assignee
        // from the selections as the assignee isn't allowed
        // to author an inaccessible entity.
        $user_storage = $this->entityTypeManager->getStorage('user');
        $assignee_user = $user_storage->load($assignee_uid);

        // Build batch for assigning author to bulk items in chunks.
        $batch_builder = (new BatchBuilder())->setTitle($this->t('Author assignment in progress'))
          ->setErrorMessage($this->t('An error occurred during author assignment.'))
          ->setFinishCallback('\Drupal\author_bulk_assignment\Batch\AuthorAssignBatch::assignAuthorFinishedCallback');
      }
      foreach ($selected as $bulk_form_key) {
        $entity = $this->loadEntityFromBulkFormKey($bulk_form_key);

        // Skip execution if current entity does not exist.
        if (empty($entity)) {
          continue;
        }

        // Skip execution if the user did not have access.
        if (!$action->getPlugin()->access($entity, $this->view->getUser())) {
          $this->messenger->addError($this->t('No access to execute %action on the @entity_type_label %entity_label.', [
            '%action' => $action->label(),
            '@entity_type_label' => $entity->getEntityType()->getLabel(),
            '%entity_label' => $entity->label(),
          ]));
          continue;
        }

        if ($bulk_author_assign) {
          if ($entity->access('update', $assignee_user)) {
            // Add individual entity as a single bulk operation
            // for assigning author and re-saving entity.
            $batch_builder->addOperation(
              '\Drupal\author_bulk_assignment\Batch\AuthorAssignBatch::assignAuthor',
              [$entity, $action, $assignee_uid]);
          }
          else {
            $key = array_search($bulk_form_key, $selected);
            $user_input[$this->options['id']][$key] = NULL;
            $this->messenger()
              ->addWarning($this->t('Failed to assign the selected author to ":title" as :author does not have required update permission for this item.', [
                ':title' => $entity->label(),
                ':author' => $assignee_user->label(),
              ]));

            continue;
          }
        }

        $count++;
        $entities[$bulk_form_key] = $entity;
      }

      // If there were entities selected but the action isn't allowed on any of
      // them, we don't need to do anything further.
      if (!$count) {
        return;
      }

      if ($bulk_author_assign) {
        // If the selected action is "bulk author assignment"
        // execute the action via batch api.
        if ($batch_builder instanceof BatchBuilder) {
          batch_set($batch_builder->toArray());
        }

        $form_state->setUserInput($user_input);
      }
      else {
        // For other action types, carry out action as normal
        // which is core default.
        // @see Drupal\views\Plugin\views\field\BulkForm::viewsFormSubmit().
        $action->execute($entities);

        $operation_definition = $action->getPluginDefinition();
        if (!empty($operation_definition['confirm_form_route_name'])) {
          $options = [
            'query' => $this->getDestinationArray(),
          ];
          $form_state->setRedirect($operation_definition['confirm_form_route_name'], [], $options);
        }
        else {
          // Don't display the message unless
          // there are some elements affected and
          // there is no confirmation form.
          $this->messenger->addStatus($this->formatPlural($count, '%action was applied to @count item.', '%action was applied to @count items.', [
            '%action' => $action->label(),
          ]));
        }
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function viewsFormValidate(&$form, FormStateInterface $form_state) {
    parent::viewsFormValidate($form, $form_state);
    $action_id = $form_state->getValue('action');
    $assignee_uid = $form_state->getValue(self::$assigneeFieldName);
    if ($action_id === $this->getEntityTypeId() . '_author_bulk_assignment_action' && !$assignee_uid) {
      $form_state->setErrorByName(self::$assigneeFieldName, $this->t('Author to assign the content to is not selected.'));
    }
  }

}
