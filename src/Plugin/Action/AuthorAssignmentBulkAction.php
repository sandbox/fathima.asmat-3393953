<?php

namespace Drupal\author_bulk_assignment\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\Plugin\Action\EntityActionBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Assigns ownership of multiple nodes to a user.
 *
 * @Action(
 *   id = "entity:author_bulk_assignment_action",
 *   action_label = @Translation("Assign bulk content to author"),
 *   deriver =
 *   "Drupal\author_bulk_assignment\Plugin\Action\Derivative\EntityAuthorAssignmentActionDeriver",
 * )
 */
class AuthorAssignmentBulkAction extends EntityActionBase {

  /**
   * Assignee aka new owner for the selected content.
   *
   * @var int
   */
  protected int $assigneeId;

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $entity->setOwnerId($this->assigneeId)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::allowedIf($account->hasPermission('assign author to selected content'));

    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * Set assignee for assigning the selected content to.
   *
   * @param int $assignee_id
   *   Assignee user ID.
   */
  public function setAssignee(int $assignee_id): void {
    $this->assigneeId = $assignee_id;
  }

}
